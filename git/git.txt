# syntax highlighting:
# http://dolphm.com/enable-syntax-highlighting-in-git/
git config --global core.pager "less -R"
git config --global color.ui true



# Create shared repo:
ssh server
cd path/above/repo
git init --bare <directory>

ssh local
cd <working directory>
git clone ssh://server/<directory>




# Unchanged:
git update-index --assume-unchanged <file>




# алиасы
git config --global alias.co checkout
git config --global alias.ci commit
git config --global alias.st status


# undo http://stackoverflow.com/questions/927358/how-do-you-undo-the-last-commit
# You want to nuke commit C and never see it again. You do this:
git reset --hard HEAD~1
# undo the commit but keep your changes
git reset HEAD~1
